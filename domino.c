#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lista.h"

int turno = 0;
int po;
Lista mesa = NULL;

int buscaMula6(Lista j1, Lista j2, Lista j3, Lista j4){
	if ((po = existeFicha(j1,6, 6)) != -1){
		Insertar(&mesa,6,6);
		return 2;
	}
	if ((po = existeFicha(j2,6, 6)) != -1){
		Insertar(&mesa,6,6);
		return 3;
	}
	if ((po = existeFicha(j3,6, 6)) != -1){
		Insertar(&mesa,6,6);
		return 4;
	}
	if ((po = existeFicha(j4,6, 6)) != -1){
		Insertar(&mesa,6,6);
		return 1;
	}
}

/*void simularjuego(){
	if(turno == 0){
		return;
	}
	switch (turno){
		case 1:

	}
}*/


int main() {
	Lista j1 = NULL;
	Lista j2 = NULL;
	Lista j3 = NULL;
	Lista j4 = NULL;
	Lista lista = NULL;
	pNodo p;
	
	srand(time(NULL));
	
	rellenamenos();
	
	generaFichas(&lista);
	
	for(int i = 1; i < 5; i++)
		generaJuegos(i);
	
	
	llenaJuegos(&j1,lista,1);
	//MostrarLista(j1, ASCENDENTE);
	llenaJuegos(&j2,lista,2);
	//MostrarLista(j2, ASCENDENTE);
	llenaJuegos(&j3,lista,3);
	//MostrarLista(j3, ASCENDENTE);
	llenaJuegos(&j4,lista,4);
	//MostrarLista(j4, ASCENDENTE);
	
	turno = buscaMula6(j1,j2,j3,j4);
	switch (turno){
		case 1: 
			Borrar(&j4,po);
			break;
		case 2: 
			Borrar(&j1,po);
			break;
		case 3: 
			Borrar(&j2,po);
			break;
		case 4: 
			Borrar(&j3,po);
			break;
	}
	MostrarLista(j1, ASCENDENTE);
	MostrarLista(j2, ASCENDENTE);
	MostrarLista(j3, ASCENDENTE);
	MostrarLista(j4, ASCENDENTE);
	
	MostrarLista(mesa, ASCENDENTE);
	//simularjuego();

	BorrarLista(&lista);
	BorrarLista(&j1);
	BorrarLista(&j2);
	BorrarLista(&j3);
	BorrarLista(&j4);

	return 0;
}