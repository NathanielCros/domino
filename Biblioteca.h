#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<stdbool.h>
#include <time.h>
#define DOMINO 28

/*Crear estructura para fichas 
  Crear las piezas (fichas) */ 
/*crea las fichas*/
struct ficha {
	int valores [2];
};

struct mesa{ /*contenedor de fichas*/
	struct ficha **fichas; //doble apuntador al arreglo donde estan las fichas en el orden en el que van saliendo no duplicar valores
 struct ficha *primero, *ultimo;
	int total; //contador para el numero de elementos total que tiene la mesa en juego 
	
};

struct mesa *M=NULL;/*apuntador al tablero*/
struct mesa *jugador1= NULL; //apuntador al jugador 1 
struct mesa *jugador2=NULL;//apuntador al jugador 2 

//Apuntador de fichas 
struct ficha *fichas = NULL;

int orden[28]; 
int ficha_actual = 0; 
int turno; 
int indice; 
int buscar; 
struct ficha *aux = NULL; 
struct mesa *jugador_actual = NULL; 
bool nadie_gano = true;
bool no_ficha = true; 
bool posicion; 

/*FUncion para crear ficha 
  devuelve apunatdor reserva memoria necesaria para una ficha  */ 
struct ficha *crear_ficha(int a, int b){
	struct ficha *nueva= calloc(sizeof(struct ficha), 1);
	nueva -> valores[0]=a;  //en su lgar 0 es igual a lado a 
	nueva -> valores[1]=b; //en su posicion 1 es igual a lado b 
	printf("creando ficha nueva [%i|%i]\n",nueva -> valores[0], nueva -> valores[1] );
return nueva;
};

/*funcion para revolver*/
void revolver (){ //Numeros no repetidos y dar fichas con numeros aleatorios
	int i=0, contador=0, valor_aleatorio;
	int arreglo_no_repetido [28];
	memset (&arreglo_no_repetido,0,sizeof(int)*DOMINO); //inicializar el arreglo de valores no repetidos 
	srand(time (NULL));
		while(i< DOMINO){
		valor_aleatorio =rand () %28; 
		if (arreglo_no_repetido[valor_aleatorio]== 0){
			orden[i]=valor_aleatorio;
			i++;
			arreglo_no_repetido[valor_aleatorio]=1;
		}
	}
}

/*
Reserva espacio para un contenedor */ 
struct mesa *nuevo_contenedor(){
	struct mesa *temp= NULL;
	temp=calloc(1, sizeof(struct mesa)); //memoria inicializada en 0 
	return temp;
};

/*agregar una ficha f en la mesa s agregar valores hasta que se nos acaben las fichas*/
void agregar_ficha (struct mesa *m, struct ficha *f){
	if (m){ //if1 
	
		if (f){ //if2 
		
			//Asignamos espacio para una ficha mas 
			m->fichas= realloc(m->fichas,sizeof(struct ficha*) * (m->total+1)); 
			//Guardamos la ficha f en el espacio creado 
			m->fichas[m->total]=f; 
			//Incrementar el contador de fichas 
			m->total++; 
			m->primero = m->fichas[0]; 
			m->ultimo = m->fichas[m->total-1]; 
		} //end de if2
		
		else {
			printf("Error en la ficha.\n"); 
		}
	} //fin de if1
	else {
	       printf("Error en el contenedor.\n"); 
	}
	
}


//Eliminar una ficha de una posicion 
struct ficha *quitar_indice(struct mesa *m, int indice){
	
	int i = indice; 
	struct ficha *temporal = NULL; 
	
	/*Importante*/ 
	                 //Variable temporal la igualamos al indice que nos pidieron 
	temporal = m->fichas[indice];
	
	while(i < m->total-1){
		
		m->fichas[i]= m->fichas[i+1];  //Eliminar las posicion 
		i++; 
	}
	
	m->fichas[i]= NULL;  //la posicion que se leimina se iguala a NULL 
	
	m->fichas = realloc(m->fichas,sizeof(struct ficha*)*(m->total-1)); //reasignamos el valor de fichas 
	m->total--;
	 if (m->total > 0){ //si tenemos fichas 
	 	m->primero = m->fichas[0]; 
	 	m->ultimo = m->fichas[m->total-1]; 
	 }
	return temporal; 
	
}

//Imprime una ficha 

void imprimir_ficha(struct ficha *f){
	
	printf("Ficha: [%d | %d ]\n",f->valores[0],f->valores[1]); 
}


int buscar_valor(struct mesa *m,int valor){
	//vamos a iterar hasta que encontrado= true 
	int v = -1; //se inicializa en -1  para indicar que 
    int t = 1; 
    int f = 0; 

	struct ficha *actual = NULL; 
	
	bool encontrado = false ; 
	
	
	int i = 0; 
	
	//Recorremos hasta las fichas hasta encontar un valor
	
	while (!encontrado && i < m->total){
		
		actual = m->fichas[i]; 
		//if (actual->fichas[i]->valores[0] == valor||actual->valores[1]== valor){
		if(actual->valores[0] == valor || actual->valores[1]==valor){
		
			
			v = i; 
		     encontrado = true; 
		}
		
		i++; 
	}
	
	
	return v; 
	
}

/* Funcion que falta implementar
void imprimir_mesa(struct mesa *m){
	int i=0; 
	//Recorremos todas las fichas de la mesa hasta encontrar un valor 
	
	while (i< m->total){
		
	}
}*/ 


/*Consultamos los valores de las orillas de la mesa*/ 
struct ficha *consultar_valores(struct mesa *m){
	
	struct ficha *f = malloc (sizeof(struct ficha));
	f->valores[0] = m->primero->valores[0]; 
	f->valores[1]= m->ultimo->valores[1]; 
	return f; 
}


/*Buscar ficha Mula*/ 

int buscar_mula(struct mesa *m, int valor){
	
	int v = -1; //Se inicializa en -1 para indicar que el valor no se ha encontrado 
	
	//struct ficha *actual = NULL; 
	struct ficha *actual = NULL; 
	
	bool encontrado = false; 
	
	int i =0; 
	
	//Recorremos todas las fichas de la mesa hasta encontrar un valor 
	printf("Buscando mula %d en mesa",valor); 
	//imprimir_mesa(m); 
	
	while (!encontrado && i < m->total){  
		actual = m->fichas[i]; 
		if (actual->valores[0]== valor && actual->valores[1]==valor){
			v = i; 
			encontrado = true; 
		}
		i++; 
	}
	 
	return v;
	
}

void agregar_ficha_orilla(struct mesa *m,struct ficha *f, bool posicion){
	//si posicion = true principio, si no final 
	if (m){
		if (f){
			//Asignamos espacio para una ficha mas 
			m->fichas = realloc(m->fichas,sizeof(struct ficha *) * (m->total+1)); 
			//Guardamos la ficha f en el espacio indicado por posicion 
			if (posicion){
				int i = m->total; 
				
				while(i>0){
					m->fichas[i] = m->fichas[i-1]; 
					i--; 
				}
				
				m->fichas[0]=f; 
			}
			else {
				//Como posicion es false, va al final y listo 
				m->fichas[m->total]=f; 
			}
			
			m->total++; 
			m->primero = m->fichas[0]; 
			m->ultimo = m->fichas[m->total-1]; 
		}
		else {
			printf("Error en la ficha.\n"); 
		}
	}
	
	
}



void voltear_ficha(struct ficha *f){
	int temporal; 
	temporal = f->valores[0]; 
	f->valores[0] = f->valores[1]; 
	f->valores [1]= temporal; 
}