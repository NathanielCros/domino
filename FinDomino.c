/**********************************************

Programa de Domino

Por:

Oscar Nathaniel
Lizbeth
Frida 
Viorel

*************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "FinDomino.h"

int main(int argc, char const *argv[]){

	for (int i = 0; i < 7; i++){
		for (int j = 0; j <= i; j++){
			fichas = realloc(fichas,(cont+1)*sizeof(struct ficha));
			crear_ficha(i,j);
			cont++;
		}
	}
	revolver();
	
	/*************** Asignamos Espacio para Jugadores ****************/
	p1 = nContenedor();
	p2 = nContenedor();
	p3 = nContenedor();
	p4 = nContenedor();
	/************************* FIN *********************************/
	/******************* Repartimos Fichas a 4 jugadores *************/
	printf("Agrega Fichas a Jugadores\n");
	for (int i = 1; i < 5; i++){
		LLenaJuegos(i);
	}
	/**************************** FIN ********************************/
	/***************************** Buscamos Mula de 6 *****************/
	for (int i = 1; i < 5; i++){
		switch (i){
			case 1:
				pa = p1;
				indice = buscarMula(p1,6);
				if (indice != -1){
					aux = quitarIndice(pa,indice);
					turno = 1;
				}
			break;
			case 2:
				pa = p2;
				indice = buscarMula(p2,6);
				if (indice != -1){
					aux = quitarIndice(pa,indice);
					turno = 2;
				}
			break;
			case 3:
				pa = p3;
				indice = buscarMula(p3,6);
				if (indice != -1){
					aux = quitarIndice(pa,indice);
					turno = 3;
				}
			break;
			case 4:
				pa = p4;
				indice = buscarMula(p4,6);
				if (indice != -1){
					aux = quitarIndice(pa,indice);
					turno = 4;
				}
			break;
		}
	}
	/**************************** FIN *********************************/
	/**************** Generamos la Mesa tiramos 1er turno *************/
	M = nContenedor(); 
	agregarFicha(M,aux); 
	printf("A jugado el jugador %d\n", turno);
	printf("Ficha jugada: "); 
	imprimirFicha(aux); 
	imprimirMesa(M); 
	/**************************** FIN ********************************/
	/********************** Simulacion Juego **************************/
	cont = 0;
	while (nGano ){
		pasa = false;
		if(turno != 4){
			turno++;
		}else {
			turno = 1;
		}

		printf("Turno del jugador %d\n",turno);
		switch (turno){
			case 1:
				pa = p1;
			break;
			case 2:
				pa = p2;
			break;
			case 3:
				pa = p3;
			break;
			case 4:
				pa = p4;
			break;
		}
		aux = consultValores(M); 
		do{
			indice = buscarValor(pa,aux->c1); 
			if(indice == -1){
				nFicha=true; 
				indice = buscarValor(pa,aux->c2); 
				if (indice  == -1){
					
					nFicha = true; 
				}
				else { //si tenemos el extremo mas derecho 
					buscar = aux->c2; 
					nFicha=false; 
					posicion = false;					
				}
			} // 
			else { //si tenemos al extremo izquierdo 
				buscar = aux->c1; 
				nFicha=false; 
				posicion = true; //izquierdo 
				
			}
			if (nFicha){
				if(cont > 4){
					printf("Se cerro el juego.\n");
					exit(0);
				}
				//Si ya no hay mas fichas el jugador pierde 
				printf("El jugador %d Pasa\n",turno);
				nFicha=false;
				pasa = true;
				cont++; 
			}
		} 
		while (nFicha);
		free(aux);  //LIBERAMOS LA FICHA AUXILIAR QUE CONSULTAMOS 
		if(!pasa){
			aux = quitarIndice(pa,indice); 
			printf("Juega"); 
			imprimirFicha(aux); 
			//printf("%d\n",(posicion) ? "Al principio" : "Al final"); 
			
			if (posicion){
				if(M->primero->c1 != aux->c2){
					voltearFicha(aux); 
				}
			}
			else {
				if(M->ultimo->c2 != aux->c1){
					voltearFicha(aux); 
				}
				
			}
			JugarFicha(M,aux,posicion);
			cont = 0;
		}
		imprimirMesa(M);
		if(pa->total==0){
			nGano = false; 
		} 
		 
	}
	 printf("\n\n\nGano el jugador %d\n\n\n",turno); 
	/***************************** FIN ********************************/

	return 0;
}