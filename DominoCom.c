/*  
     Juego de Domino 
     Crear Estructura para una ficha 
     Crear todas las piezas del domino parsa ser usadas durante el juego 
     Crear estructura para el tablero del juego 
     Revolver 
     -Mostrar fichas en el orden aleatorio 
     Girar 
     Estructura para jugador
     
    
    */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<stdbool.h>
#include <time.h>
#include "Bdomino.h"

#define DOMINO 28

int main(){
	struct ficha *temporal;
	int busqueda; 
	int contador =0; //contador de fichas independiente 
	int i=0, j=0;
	//Inicializar fichas con valores 0 a 6 de cada lado 
	while (i<=6){
		j=0;
		while(j<=i){
			fichas= realloc(fichas,(contador + 1)*sizeof(struct ficha)); //reservar memoria par la cantidad de fichas 
			temporal = crear_ficha (i,j);
			memcpy(&fichas[contador], temporal, sizeof (struct ficha)); //Copiar lo que se encuentra en la variable temporal 
	       	free (temporal);                                            //en el arreglo de fichas 
			contador++;
			j++;	
		}
		i++;
	}
	printf("total de fichas creadas %i \n",contador );
	revolver();
	printf("\n\nfichas en desorden\n");
	i=0;
	
	while(i<contador){
		printf("ficha:  [%i|%i]\n", fichas[orden [i]].valores[0],fichas[orden [i]]. valores[1]); //orden es el aleatorio 
		i++;
	}
	
	/*Asignamos espacio a los jugadores*/ 
	jugador1 = nuevo_contenedor(); 
	jugador2 = nuevo_contenedor(); 
	
	/* Repartimos fichas entre los dos jugadores*/ 
	printf("Agregamos ficha al jugador 1\n"); 
	i=0; 
	
	while (i <6){
		
		agregar_ficha(jugador1,&fichas[orden[i]]); 
		imprimir_ficha(&fichas[orden[i]]); 
		i++; 
	}
	
	printf("Agregamos ficha al jugador 2\n"); 
	
	while ( i < 12){
		agregar_ficha(jugador2,&fichas[orden[i]]); 
		imprimir_ficha(&fichas[orden[i]]); 
		i++; 
	}
	
	turno = rand() % 2;  //jugador 1 o jugador 2  que turno le corresponde jugar 
	
	jugador_actual = ((turno) % 2) ? jugador2 : jugador1; 
	printf("Inicia el jugador %d\n",turno + 1); 
	indice = -1; 
	buscar = 6; 
	printf("Buscando Mula\n"); 
	                                        //mientras el indice no devuelva un valo verdadero vamos a seguir buscando 
	while (indice == -1 && buscar >=0){
		indice = buscar_mula(jugador_actual,buscar); 
		printf("Buscando Mulda %d: resultado %d\n",buscar,indice); 
		buscar--; 
	}
	
	if (indice != -1){ //si encontro al menos una mula buscar+1
		
		aux = quitar_indice(jugador_actual,indice); 
		
	}
	else { //No encontro mula cede el turno al jugador 2 
	
	turno++; //cedemos el turno al siguiente jugador 
	printf("Cambiando de jugador %d\n",(turno %2) + 1); 
	jugador_actual = ((turno) % 2) ? jugador2 : jugador1; 
	//indice = -1; 
	buscar = 6; 
	
	while (indice == -1 && buscar >=0){
		indice = buscar_mula(jugador_actual,buscar); 
		printf("Buscando Mula %d: resultado %d\n",buscar,indice); 
	}
	
	if (indice != -1){ //Si encontro al menos una mula (buscar + 1) 
		
		aux = quitar_indice(jugador_actual,indice); 
		
		
	}
	 else {
	 	
	 	//Ningun jugador tiene Mula, por lo tanto finaliza el juego
		 
		 printf("No hay mula finaliza el juego. Nadie gana\n"); 
		 exit(0);  
	 }
		
	}
	//el programa enncotro una mula 
	M = nuevo_contenedor(); 
	agregar_ficha(M,aux); 
	printf("A jugado el jugador %d\n",(turno%2)+1);
	printf("Ficha jugada: "); 
	imprimir_ficha(aux); 
	printf("Mesa actual: "); 
	//imprimir_mesa(M); 
	
	while (nadie_gano ){
		turno++; 
		printf("Turno del jugador %d\n",(turno%2)+1);
		jugador_actual = ((turno) % 2) ? jugador1 : jugador1; 
		aux = consultar_valores (M); 
		
		do{
			indice = buscar_valor(jugador_actual,aux->valores[0]); 
			if(indice == -1){
				no_ficha=true; 
				indice = buscar_valor(jugador_actual,aux->valores[1]); 
				if (indice  == -1){
					
					no_ficha = true; 
				}
				else { //si tenemos el extremo mas derecho 
				
				buscar = aux->valores[1]; 
				no_ficha=false; 
				posicion = false; 
					
				}
			} // 
			else { //si tenemos al extremo izquierdo 
			buscar = aux->valores[0]; 
			no_ficha=false; 
			posicion = true; //izquierdo 
				
			}
			if (no_ficha){
				//no hay extremos 
				
				if (i< 28){
					printf("El jugador no tiene fichas para jugar agregando una"); 
					imprimir_ficha(&fichas[orden[i]]); 
					agregar_ficha(jugador_actual,&fichas[orden[i]]); 
					i++; 
				}
				else{
					//Si ya no hay mas fichas el jugador pierde 
					printf("El jugador %d Pierde\n",(turno%2)+1); 
					exit (0); 
				}
			}
		} 
		while (no_ficha);
		free(aux);  //LIBERAMOS LA FICHA AUXILIAR QUE CONSULTAMOS 
		aux = quitar_indice(jugador_actual,indice); 
		printf("Juega"); 
		imprimir_ficha(aux); 
		printf("%d\n",(posicion) ? "Al principio" : "Al final"); 
		
		if (posicion){
			if(M->primero->valores[0] != aux->valores[1]){
				voltear_ficha(aux); 
			}
		}
		else {
			if(M->ultimo->valores[1] != aux->valores[0]){
				voltear_ficha(aux); 
			}
			
		}
		agregar_ficha_orilla(M,aux,posicion); 
		printf("Mesa Actual: "); 
		//imprimir_mesa(M); //funcion que falta por hacer 
		if(jugador_actual->total==0){
			nadie_gano = false; 
		} 
		 
	}
	 printf("Gano el jugador %d\n",(turno%2)+1); 
	return 0;
}
