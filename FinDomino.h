/***********************************

Libreria para Domino

Por: 

Oscar Nathaniel
Lizbeth
Frida
Viorel

*************************************/

/************* Se crea la Estructura **************/
typedef struct ficha{
	int c1, c2;
} Ficha;
typedef struct mesa{
	Ficha **fichas, *primero, *ultimo;
	int total;
} Mesa;
/*************** Fin *********************/

/**************** Variables Globales ****************/

Ficha *fichas = NULL, *aux = NULL;
Mesa *M = NULL, *p1 = NULL, *p2=NULL, *p3=NULL, *p4=NULL, *pa = NULL;
int cont = 0, orden[28], ficha_actual = 0, turno = -1, indice, buscar;
bool nGano = true, nFicha = true, posicion, pasa = false;

/***************** Fin ****************************/
/********************** Prototipos *********************/
void crear_ficha(int,int);
void revolver();
void imprimirFichas();
void imprimirFicha(Ficha *);
void imprimirMesa(Mesa *);
Mesa *nContenedor();
void agregarFicha(Mesa *, Ficha *);
void JugarFicha(Mesa *,Ficha *,bool);
Ficha *quitarIndice(Mesa *, int);
int buscarValor(Mesa *,int);
int buscarMula(Mesa *,int);
void LLenaJuegos(int);
Ficha *consultValores(Mesa *);
void voltearFicha(Ficha *);
/*************************** Fin ************************/

/************************ Funciones **********************/ 
void crear_ficha(int a, int b){
	Ficha *nueva = calloc(sizeof(struct ficha),1);
	nueva->c1=a;
	nueva->c2=b;
	memcpy(&fichas[cont],nueva,sizeof(struct ficha));
	//printf("Crea ficha nueva [%i|%i]\n",fichas[cont].c1,fichas[cont].c2);
	free(nueva);
};

void revolver (){
	int i=0, contador=0, random;
	int no_repet[28];
	//LLena el arrelgo no repetido en 0
	memset (&no_repet,0,sizeof(int)*28);
	srand(time (NULL));
		while(i< 28){
		random =rand () %28; 
		if (no_repet[random]== 0){
			orden[i]=random;
			i++;
			no_repet[random]=1;
		}
	}
}
void imprimirFichas(){
	printf("Fichas Revueltas:\n");
	for (int i = 0; i < 28; i++){
		printf("Ficha: [%i|%i]\n", fichas[orden[i]].c1, fichas[orden[i]].c2);
	}
}
void imprimirFicha(Ficha *f){
	printf("Ficha [%i|%i]\n",f->c1,f->c2);
}
void imprimirMesa(Mesa *m){
	printf("Mesa actual: ");
	for (int i = 0; i < m->total; i++){
			printf("[%i|%i]", m->fichas[i]->c1,m->fichas[i]->c2);
	}
	printf("\n");
}
Mesa *nContenedor(){
	Mesa *tmp = NULL;
	tmp = calloc(1,sizeof(Mesa));
	return tmp;
}
void agregarFicha(Mesa *m, Ficha *f){
	if(m){
		if(f){
			//Espacio fichas
			m->fichas = realloc(m->fichas,sizeof(Ficha *) * (m->total+1));
			//Guardamos ficha f en el espacio
			m->fichas[m->total] = f;
			//Se incrementa el contador
			m->total++;
			m->primero = m->fichas[0];
			m->ultimo = m->fichas[m->total-1];
		}else {
			printf("Error en la ficha\n");
		}
	}else{
		printf("Error en el contenedor\n");
	}
}
void JugarFicha(Mesa *m,Ficha *f,bool pos){
	if(m){
		if(f){
			m->fichas = realloc(m->fichas,sizeof(Ficha *) * (m->total+1));
			if(pos){
				int i = m->total;
				while(i>0){
					m->fichas[i] = m->fichas[i-1]; 
					i--; 
				}
				m->fichas[0] = f;
			}else{
				m->fichas[m->total] = f;
			}
			m->total++;
			m->primero = m->fichas[0];
			m->ultimo = m->fichas[m->total-1];
		}else {
			printf("Error en la ficha\n");
		}
	}else{
		printf("Error en el contenedor\n");
	}
}
Ficha *quitarIndice(Mesa *m, int indx){
	int i = indx;
	Ficha *tmp = NULL;
	tmp = m->fichas[indx];
	while(i < (m->total-1)){
		m->fichas[i] = m->fichas[i+1];
		i++;
	}
	m->fichas[i] = NULL;
	m->fichas = realloc(m->fichas,sizeof(Ficha*)* (m->total-1));
	m->total--;
	if(m->total > 0){
		m->primero = m->fichas[0];
		m->ultimo = m->fichas[m->total-1];
	}
	return tmp;
}
int buscarValor(Mesa *m,int valor){
	int v = -1, i = 0;
	Ficha *actual = NULL;
	bool mtch = false;

	while(!mtch && i < m->total){
		actual = m->fichas[i];
		if (actual->c1 == valor || actual->c2 == valor){
			v = i;
			mtch = true;
		}
		i++;
	}
	return v;
}
int buscarMula(Mesa *m,int valor){
	int v = -1, i = 0;
	Ficha *actual = NULL;
	bool mtch = false;

	while(!mtch && i < m->total){
		actual = m->fichas[i];
		if (actual->c1 == valor && actual->c2 == valor){
			v = i;
			mtch = true;
		}
		i++;
	}
	return v;
}
void LLenaJuegos(int j){
	switch(j){
		case 1:
			printf("\n\n Jugador 1\n");
			for (int i = 0; i <= 6; i++){
				agregarFicha(p1,&fichas[orden[i]]);
				imprimirFicha(&fichas[orden[i]]);
			}
		break;
		case 2:
			printf("\n\n Jugador 2\n");
			for (int i = 7; i <= 13; i++){
				agregarFicha(p2,&fichas[orden[i]]);
				imprimirFicha(&fichas[orden[i]]);
			}
		break;
		case 3:
			printf("\n\n Jugador 3\n");
			for (int i = 14; i <= 20; i++){
				agregarFicha(p3,&fichas[orden[i]]);
				imprimirFicha(&fichas[orden[i]]);
			}
		break;
		case 4:
			printf("\n\n Jugador 4\n");
			for (int i = 21; i <= 27; i++){
				agregarFicha(p4,&fichas[orden[i]]);
				imprimirFicha(&fichas[orden[i]]);
			}
		break;	
	}
}
Ficha *consultValores(Mesa *m){
	Ficha *f = malloc(sizeof(Ficha));
	f->c1 = m->primero->c1;
	f->c2 = m->ultimo->c2;
	return f;
}
void voltearFicha(Ficha *f){
	int tmp;
	tmp = f->c1;
	f->c1 = f->c2;
	f->c2 = tmp;
}