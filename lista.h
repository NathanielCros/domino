#define ASCENDENTE 1
#define DESCENDENTE 0

//Arreglos para jugadores se insertaran indices para las listas
int numj1[7], numj2[7], numj3[7], numj4[7];

//Esctutura con 3 datos valor1, valor 3 y pos
typedef struct _nodo {
   int valor1, valor2, pos;
   struct _nodo *siguiente;
   struct _nodo *anterior;
} tipoNodo;

//Se definen 3 estructuras
typedef tipoNodo *pNodo;
typedef tipoNodo *Lista;

//Inserta un nuevo valor a la lista
void Insertar(Lista *lista, int v1, int v2) {
   pNodo nuevo, actual;
   int pos = 1;

   /* Crear un nodo nuevo */
   nuevo = (pNodo)malloc(sizeof(tipoNodo));
   nuevo->valor1 = v1;
   nuevo->valor2 = v2;
   
   /* Colocamos actual en la primera posición de la lista */
   actual = *lista;
   if(actual) while(actual->anterior) actual = actual->anterior;
   /* Si la lista está vacía o el primer miembro es mayor que el nuevo */
   if(!actual) {
      /* Añadimos la lista a continuación del nuevo nodo */
      nuevo->pos = 0;
      nuevo->siguiente = actual; 
      nuevo->anterior = NULL;
      if(actual) actual->anterior = nuevo;
      if(!*lista) *lista = nuevo;
   }
   else {
      /* Avanzamos hasta el último elemento o hasta que el siguiente tenga 
         un valor mayor que v */
      while(actual->siguiente){
         pos++; 
         actual = actual->siguiente;
      }
      nuevo->pos = pos;
      /* Insertamos el nuevo nodo después del nodo anterior */
      nuevo->siguiente = actual->siguiente;
      actual->siguiente = nuevo;
      nuevo->anterior = actual;
      if(nuevo->siguiente) nuevo->siguiente->anterior = nuevo;
   }
}
// Borra elementos de la lista con la posision
int Borrar(Lista *lista, int v) {
   pNodo nodo;
   
   /* Buscar el nodo de valor v */
   nodo = *lista;
   while(nodo && nodo->pos < v) nodo = nodo->siguiente;
   while(nodo && nodo->pos > v) nodo = nodo->anterior;
   /* El valor v no está en la lista */

   if(!nodo || nodo->pos != v) return -1;
   
   /* Borrar el nodo */
   /* Si lista apunta al nodo que queremos borrar, apuntar a otro */
   if(nodo == *lista)
     if(nodo->anterior) *lista = nodo->anterior;
     else *lista = nodo->siguiente;
   
   if(nodo->anterior) /* no es el primer elemento */
      nodo->anterior->siguiente = nodo->siguiente;
   if(nodo->siguiente) /* no es el último nodo */
      nodo->siguiente->anterior = nodo->anterior;
   free(nodo);
   return 0;
}
//Libera la memoria ocupada por la lista
void BorrarLista(Lista *lista) {
   pNodo nodo, actual;

   actual = *lista;
   while(actual->anterior) actual = actual->anterior;

   while(actual) {
      nodo = actual;
      actual = actual->siguiente;
      free(nodo);
   }
   *lista = NULL;
}
//Muestra todos los datos de la lista
void MostrarLista(Lista lista, int orden) {
   pNodo nodo = lista;

   if(!lista) printf("Lista vacía");

   nodo = lista;
   if(orden == ASCENDENTE) {
      while(nodo->anterior) nodo = nodo->anterior;
      printf("Orden ascendente: \n");
      while(nodo) {
         printf("%d .-|%d | %d|\n",nodo->pos, nodo->valor1, nodo->valor2);
         nodo = nodo->siguiente;
      }
   }
   else {
      while(nodo->siguiente) nodo = nodo->siguiente;
      printf("Orden descendente: \n");
      while(nodo) {
         printf("%d .-|%d | %d|\n",nodo->pos, nodo->valor1, nodo->valor2);
         nodo = nodo->anterior;
      }
   }
   
   printf("\n");
}
//Esta funcion regresa una Ficha
pNodo getNumero(Lista lista,int v){
   pNodo nodo;
   
   /* Buscar el nodo de valor v */
   nodo = lista;
   while(nodo && nodo->pos < v) nodo = nodo->siguiente;
   while(nodo && nodo->pos > v) nodo = nodo->anterior;
   /* El valor v no está en la lista */

   if(!nodo || nodo->pos != v) return NULL;

   return nodo;
}
//Regresa la posision de una ficha especifica
int existeFicha(Lista lista, int v1, int v2){
   pNodo nodo;
   /* Buscar el nodo de valor v */
   nodo = lista;
   while(nodo->siguiente != NULL){
      if(nodo->valor1 == v1)
         if(nodo->valor2 == v2){
            printf("%d\n", nodo->pos);
            return nodo->pos;
         }
      nodo = nodo->siguiente;
      cont++;
   }
   
   return -1;
}
//regresa una fica con la el promer valor de la ficha
pNodo getFichaV1(Lista lista, int v1){
   pNodo nodo;
   nodo = lista;
   while(nodo && nodo->valor1 < v1) nodo = nodo->siguiente;
   while(nodo && nodo->valor1 > v1) nodo = nodo->anterior;
   /* El valor v no está en la lista */

   if(!nodo || nodo->valor1 != v1) return NULL;

   return nodo;
}
//regresa una fica con la el segundo valor valor de la ficha
pNodo getFichaV2(Lista lista, int v2){
   pNodo nodo;
   nodo = lista;
   while(nodo && nodo->valor1 < v2) nodo = nodo->siguiente;
   while(nodo && nodo->valor1 > v2) nodo = nodo->anterior;
   /* El valor v no está en la lista */

   if(!nodo || nodo->valor1 != v2) return NULL;

   return nodo;
}
//Imprimr Juegos de los arreglos
void imprimirJuegos(){

   printf("Primer Juego\n");
   for(int i = 0; i < 7; i++){
      printf("%d\n", numj1[i]);
   }
   printf("Segundo Juego\n");
   for(int i = 0; i < 7; i++){
      printf("%d\n", numj2[i]);
   }
   printf("Tercer Juego\n");
   for(int i = 0; i < 7; i++){
      printf("%d\n", numj3[i]);
   }
   printf("Cuarto Juego\n");
   for(int i = 0; i < 7; i++){
      printf("%d\n", numj4[i]);
   }
}
//Rellenamos valors de arreglo con -1 para que no esten en null
void rellenamenos(){
   for(int i = 0; i < 7; i++){
      numj1[i] = -1;
   }
   for(int i = 0; i < 7; i++){
      numj2[i] = -1;
   }
   for(int i = 0; i < 7; i++){
      numj3[i] = -1;
   }
   for(int i = 0; i < 7; i++){
      numj4[i] = -1;
   }
}
//Generamos las Fichas en la Lista principal
void generaFichas(Lista *lista){
   for(int i = 0; i < 7; i++)
      for(int j = i; j < 7; j++)
         Insertar(lista, i , j);
}
//Rectificamos si el numero random existe en algun arreglo de los jugadores
int existe(int n){
   for(int i = 0; i < 7; i++){
      if(numj1[i] == n){
         return 1;
      }
   }
   for(int i = 0; i < 7; i++){
      if(numj2[i] == n){
         return 1;
      }
   }
   for(int i = 0; i < 7; i++){
      if(numj3[i] == n){
         return 1;
      }
   }
   for(int i = 0; i < 7; i++){
      if(numj4[i] == n){
         return 1;
      }
   }
   return 0;
}
//Se llenan los arreglos con numero que seran posisiones de una lista con las fichas
void generaJuegos(int j){
   int cont = 0, num;
   do{
      num = rand() % 28;
      //imprimirJuegos();
      if(existe(num) != 1){
         switch (j){
            case 1: 
               numj1[cont] = num;
               cont++;
               break;
            case 2: 
               numj2[cont] = num;
               cont++;
               break;
            case 3: 
               numj3[cont] = num;
               cont++;
               break;
            case 4: 
               numj4[cont] = num;
               cont++;
               break;
         }

      }

   }while(cont != 7);
}
//Con los arreglos llenamos 4 listas de los jugadores
void llenaJuegos(Lista *j, Lista lista, int ju){
   pNodo n;
   switch (ju){
      case 1: 
         for(int i = 0; i < 7; i++){
            n = getNumero(lista, numj1[i]);
            if(n != NULL){
               Insertar(j,n->valor1,n->valor2);
            }
         }
         break;
      case 2: 
         for(int i = 0; i < 7; i++){
            n = getNumero(lista, numj2[i]);
            if(n != NULL){
               Insertar(j,n->valor1,n->valor2);
            }
         }
         break;
      case 3: 
         for(int i = 0; i < 7; i++){
            n = getNumero(lista, numj3[i]);
            if(n != NULL){
               Insertar(j,n->valor1,n->valor2);
            }
         }
         break;
      case 4: 
         for(int i = 0; i < 7; i++){
            n = getNumero(lista, numj4[i]);
            if(n != NULL){
               Insertar(j,n->valor1,n->valor2);
            }
         }
         break;
      }
   
}